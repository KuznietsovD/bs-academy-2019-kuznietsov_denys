const assert = require('assert');
const credentials = require("../credentials.json");

const MenuActions = require('../Menu/actions/menu_pa');
const MyListsActions = require('./actions/list_pa');

const menuSteps = new MenuActions();
const listSteps = new  MyListsActions();

function userLogin(email, password) {
    const emailField = $('input[name="email"]');
    const passwordField = $('input[type="password"]');
    const loginButton = $('button.is-primary');

    emailField.clearValue();
    emailField.setValue(email);
    passwordField.clearValue();
    passwordField.setValue(password);
    loginButton.click();
}

function waitForLoader() {
    const loader = $('div#preloader');
    loader.waitForDisplayed(10000);
    loader.waitForDisplayed(10000, true);
}

function getNotification() {
    const notification = $('div.toast div');
    return notification.getText();
}

describe('Hedonist my lists section', () => {
   
    beforeEach(() => {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create list', () => {

        userLogin(credentials.login.validEmail, credentials.login.validPassword);
        waitForLoader();

        menuSteps.navigateToLists();
        waitForLoader();

        listSteps.createList();
        listSteps.enterListName(credentials.newList.name);
        listSteps.enterLocation(credentials.newList.location);
        listSteps.enterPlace(credentials.newList.Place);
        listSteps.confirmPlace();
        listSteps.saveList();
        waitForLoader();

        assert.strictEqual(getNotification(), "The list was saved!");

        //postconditions
        listSteps.deletList();
        listSteps.confirmDelete();
        waitForLoader();

        assert.strictEqual(getNotification(), "The list was removed");
    });

    it('should delete list', () => {

        userLogin(credentials.login.validEmail, credentials.login.validPassword);
        waitForLoader();

        menuSteps.navigateToLists();
        waitForLoader();

        listSteps.createList();
        listSteps.enterListName(credentials.newList.name);
        listSteps.enterLocation(credentials.newList.location);
        listSteps.enterPlace(credentials.newList.Place);
        listSteps.confirmPlace();
        listSteps.saveList();
        waitForLoader();

        assert.strictEqual(getNotification(), "The list was saved!");

        listSteps.deletList();
        listSteps.confirmDelete();
        waitForLoader();

        assert.strictEqual(getNotification(), "The list was removed");
    });
});