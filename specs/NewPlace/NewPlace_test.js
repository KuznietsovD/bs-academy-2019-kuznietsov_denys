const assert = require('assert');
const credentials = require("../credentials.json");
const path = require('path');

const MenuActions = require('../Menu/actions/menu_pa');
const PlaceActions = require('../NewPlace/actions/place_pa');

const menuSteps = new MenuActions();
const placeSteps = new PlaceActions();

function userLogin(email, password) {
    const emailField = $('input[name="email"]');
    const passwordField = $('input[type="password"]');
    const loginButton = $('button.is-primary');

    emailField.clearValue();
    emailField.setValue(email);
    passwordField.clearValue();
    passwordField.setValue(password);
    loginButton.click();
}

function waitForLoader() {
    const loader = $('div#preloader');
    loader.waitForDisplayed(10000);
    loader.waitForDisplayed(10000, true);
}

describe('Hedonist new place section', () => {
   
    beforeEach(() => {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250/');
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create place', () => {

        userLogin(credentials.login.validEmail, credentials.login.validPassword);
        waitForLoader();
      
        menuSteps.navigateToNewPlace();

        placeSteps.enterName(credentials.newPlace.name);
        placeSteps.enterCity(credentials.newPlace.city);
        placeSteps.enterZip(credentials.newPlace.zip);
        placeSteps.enterAddress(credentials.newPlace.address);
        placeSteps.enterPhoneNumber(credentials.newPlace.phone);
        placeSteps.enterWebsite(credentials.newPlace.website);
        placeSteps.enterDescription(credentials.newPlace.description);
        placeSteps.nextButton1();

        placeSteps.uploadPhoto(path.join(__dirname,credentials.newPlace.photoPath));
        placeSteps.nextButton2();

        placeSteps.nextButton3();
        placeSteps.selectCategory();
        placeSteps.selectTag();
        placeSteps.nextButton4();

        placeSteps.selectFeature();
        placeSteps.nextButton5();
        placeSteps.nextButton6();
        placeSteps.nextButton7();

        waitForLoader();

        assert.equal(placeSteps.getCreatedPlaceName(), credentials.newPlace.name);
    });

});