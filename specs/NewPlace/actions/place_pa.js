const PlacePage = require('../page/place_po');
const place = new PlacePage();

class PlaceActions {

    enterName(value) {
        place.nameInput.waitForDisplayed(2000);
        place.nameInput.clearValue();
        place.nameInput.setValue(value);
    }

    enterCity(value) {
        place.cityInput.waitForDisplayed(2000);
        place.cityInput.clearValue();
        place.cityInput.setValue(value);
        place.cityConfirm.waitForDisplayed(5000);
        place.cityConfirm.click(); 
    }
    
    enterZip(value) {
        place.zipInput.waitForDisplayed(2000);
        place.zipInput.clearValue();
        place.zipInput.setValue(value);
    }

    enterAddress(value) {
        place.addressInput.waitForDisplayed(2000);
        place.addressInput.clearValue();
        place.addressInput.setValue(value);
    }

    enterPhoneNumber(value) {
        place.phoneNumberInput.waitForDisplayed(2000);
        place.phoneNumberInput.clearValue();
        place.phoneNumberInput.setValue(value);
    }

    enterWebsite(value) {
        place.websiteInput.waitForDisplayed(2000);
        place.websiteInput.clearValue();
        place.websiteInput.setValue(value);
    }

    enterDescription(value) {
        place.descriptionInput.waitForDisplayed(2000);
        place.descriptionInput.clearValue();
        place.descriptionInput.setValue(value);
    }

    nextButton(button) {
        button.waitForDisplayed(2000);
        browser.pause(500);
        button.click()
    }

    nextButton1() {
        this.nextButton(place.nextButton[0]);
    }

    uploadPhoto(value) {
        place.nextButton[1].waitForDisplayed(2000);
        browser.pause(500);
        place.uploadField.setValue(value);
    }

    nextButton2() {
        this.nextButton(place.nextButton[1]);
    }

    nextButton3() {
        this.nextButton(place.nextButton[2]);
    }

    selectCategory() {
        browser.pause(500);
        place.selectCategorysMenu.waitForDisplayed(2000);
        place.selectCategorysMenu.click();
        place.selectCategory.waitForDisplayed(5000);
        place.selectCategory.click();
    }

    selectTag() {
        place.selectTagsMenu.waitForDisplayed(5000);
        place.selectTagsMenu.click();
        place.selectTag.waitForDisplayed(5000);
        place.selectTag.click();
       
    }

    nextButton4() {
        this.nextButton(place.nextButton[3]);
    }

    selectFeature() {
        place.selectFeature.waitForDisplayed(5000);
        browser.pause(500);
        place.selectFeature.click();
    }

    nextButton5() {
        this.nextButton(place.nextButton[4]);
    }

    nextButton6() {
        this.nextButton(place.nextButton[5]);
    }

    nextButton7() {
        this.nextButton(place.nextButton[6]);
    }

    getCreatedPlaceName() {
        place.createdPlaceName.waitForDisplayed(2000);
        browser.pause(500);
        return place.createdPlaceName.getText();
    }
    
    
};

module.exports = PlaceActions;